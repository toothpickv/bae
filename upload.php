<?php
/*
* 上传后图片处理
* Kslrwang@gmail.com
*/
if ($_POST) {
# 接受字符串并保存为图片，名字按照当前月天时分秒
$base64 = $_POST['data'];
$IMG = base64_decode( $base64 );
$name = date("mdths");
$img_name = "images/".$name.".png";
file_put_contents($img_name, $IMG);

# Curl函数
function Curl($url,$post_data,$i) {
    $ch = curl_init();  
    curl_setopt($ch, CURLOPT_POST, 1);  
    curl_setopt($ch, CURLOPT_URL,$url);
    if ($i == true) {
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    }
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
    ob_start();  
    curl_exec($ch);  
    $result = ob_get_contents();
    ob_end_clean();
    return $result;
}

$client_id = "";				# API Key
$client_secret = "";	# Secret Key
$post_string = "grant_type=client_credentials&client_id=$client_id&client_secret=$client_secret"; #组织Token参数
$oauth_bd = Curl("http://openapi.baidu.com/oauth/2.0/token?", $post_string, "0");	#请求Token
$json_token = json_decode($oauth_bd);	#编码为数组
$access_token = urlencode($json_token->access_token);	#获得其中认证成功token并编码
$url = urlencode("http://www.kslr.net/htmlgra/images/$name.png");	#需要被识别的图片地址
$get_gra_json = Curl("https://openapi.baidu.com/rest/2.0/media/v1/face/detect?", "access_token=$access_token&url=$url" , true);#请求处理

#返回错误友好处理
$get_gra = json_decode($get_gra_json, true);
if (isset($get_gra['error_code'])) {
	if ($get_gra['error_code'] == "70001") {
		echo "请求参数错误!";
	} elseif ($get_gra['error_code'] == "70503") {
		echo "服务不可用!";
	} elseif ($get_gra['error_code'] == "70131") {
		echo "内部处理错误!";
	} elseif ($get_gra['error_code'] == "70132") {
		echo "不能访问图片!";
	} elseif ($get_gra['error_code'] == "70133") {
		echo "人脸识别配额不足!";
	}
	echo $url;
} else {
	#正常处理
	$img_width = $get_gra['img_width']; #图宽度
	$img_hight = $get_gra['img_height']; #图高度
	$Gender = $get_gra['face']['0']['attribute']['gender']['value']; #性别
	if ($Gender == "female") {
		echo "女性";
	} elseif ($Gender == "male") {
		echo "男性";
	} else {
		echo "未知";
	}
	echo "，正确度".$get_gra['face']['0']['attribute']['smiling']['confidence'] / "1.0" * "100"."%";
}

} else {
	echo "未提交有效数据";
}
?>