摄像功能是使用的HTML5 API，界面使用的Bootstrap，程序使用PHP写的，很简单的一个程序。

使用需要设置API Key和Secret Key以及图片URL，这些设置都是在Upload.php文件中。

>代码结构
index.html	 负责截图上传和显示处理结果
uoload.php 	 接受图片通过API提交图片最后显示返回值
view.php  	 显示目录下所有文件，5S自动刷新一次。

>注意：
需要自己新建一个images目录用来存放图片；
没有实现图片定期清理功能

演示地址：http://www.kslr.net/htmlgra